variable "DOCKER_IMG_TAG" {}
variable "CIRCLE_BUILD_NUM" {}
variable "environment" {
  type        = string
  description = "environment"
  default     = "mbh-banking"
}

terraform {
  backend "s3" {
    bucket = "mbh.terraform.configs"
    key    = "ms-node-prototype.tfstate"
    region = "eu-central-1"
  }
}

module "microservice" {
  source              = "git@github.com:Magyar-Bankholding-Zrt/infra-module.git"
  service_name        = "ms-node-prototype"
  service_version     = "1"
  service_image       = "mbhsys/ms-node-prototype:${var.DOCKER_IMG_TAG}"
  service_replicas    = "2"
  cluster_environment = var.environment
  pod_version         = var.CIRCLE_BUILD_NUM
  memory_limit        = "512Mi"
}

#output "service_host_name" {
#  value = "${module.microservice.service_host_name}"
#}
