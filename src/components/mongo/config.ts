const mongoConfig = {
  host: process.env.MONGO_HOST || "0.0.0.0",
  port: process.env.MONGO_PORT || "27017",
  username: process.env.MONGO_USERNAME || "root",
  password: process.env.MONGO_PASSWORD || "password",
  database: process.env.MONGO_DATABASE || "mkb",
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: false, // Don't build indexes
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 as const, // Use IPv4, skip trying IPv6
  },
};

export default mongoConfig;
