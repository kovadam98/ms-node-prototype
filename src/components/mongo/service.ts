import mongoose, { ConnectOptions } from "mongoose";
import config from "./config";

const mongoService = {
  start: async () => {
    const url = `mongodb://${config.username}:${config.password}@${config.host}:${config.port}`;
    const options: ConnectOptions = config.options;

    return await mongoose.connect(url, options);
  },

  stop: async () => {
    return await mongoose.disconnect();
  },
};

export default mongoService;
