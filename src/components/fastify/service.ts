import Fastify from "fastify";
import config from "./config";
import { Route } from "./schema";

const fastify = Fastify({ logger: true });

const fastifyService = {
  // convenience method
  start: async (routes: Route<any>[]) => {
    routes.forEach((route) => fastify.route(route));
    await fastify.listen(config.port, config.host);
  },

  // convenience method
  stop: async () => {
    await fastify.close();
  },
};

export const logger = fastify.log;

export default fastifyService;
