import { RawServerBase, RouteHandlerMethod, RouteOptions } from "fastify";
import { IncomingMessage, Server, ServerResponse } from "http";

export type Handler<RequestSchema> = RouteHandlerMethod<
  RawServerBase,
  IncomingMessage,
  ServerResponse,
  RequestSchema
>;

export type Route<RequestSchema> = RouteOptions<
  Server,
  IncomingMessage,
  ServerResponse,
  RequestSchema
>;
