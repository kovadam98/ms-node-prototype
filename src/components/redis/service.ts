import Redis from "ioredis";
import redisConfig from "./config";

const redisService = new Redis(redisConfig);

export default redisService;
