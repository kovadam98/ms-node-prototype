const redisConfig = {
  host: process.env.REDIS_HOST || "0.0.0.0", // Redis host
  port: Number(process.env.REDIS_PORT) || 6379, // Redis port
  family: 4, // 4 (IPv4) or 6 (IPv6),
  lazyConnect: true,
};

export default redisConfig;
