import { Static, Type } from "@sinclair/typebox";
import { RouteGenericInterface } from "fastify/types/route";

// This is necessary

const response = Type.Object({
  status: Type.String(),
});

const params = Type.Object({
  transactionId: Type.String(),
});

// This is template

export const requestSchema = {
  params: params,
  response: {
    200: response,
  },
};

export interface RequestSchema extends RouteGenericInterface {
  Params: Static<typeof params>;
  Response: Static<typeof response>;
}
