import { RequestSchema, requestSchema } from "./schema";
import { Handler, Route } from "../../../components/fastify/schema";
import redisService from "../../../components/redis/service";

// Handler
export const getTransactionStatusHandler: Handler<RequestSchema> = async (
  request,
  reply
) => {
  const obj = await redisService.get(request.params.transactionId);
  const status = obj ? "OK" : "NOT OK";
  console.log(obj);
  reply.send({ status });
};

// Route
export const getTransactionStatusRoute: Route<RequestSchema> = {
  method: "GET",
  url: "/api/v1/getTransactionStatus/:transactionId",
  schema: requestSchema,
  handler: getTransactionStatusHandler,
};
