import { Static, Type } from "@sinclair/typebox";
import { RouteGenericInterface } from "fastify/types/route";

// This is necessary

const response = Type.Object({
  status: Type.String(),
});

// This is template

export const requestSchema = {
  response: {
    200: response,
  },
};

export interface RequestSchema extends RouteGenericInterface {
  Response: Static<typeof response>;
}
