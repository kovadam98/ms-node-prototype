import { RequestSchema, requestSchema } from "./schema";
import { Handler, Route } from "../../../components/fastify/schema";

// Handler
export const getHealthHandler: Handler<RequestSchema> = async (
  request,
  reply
) => {
  reply.send({ status: "OK" });
};

// Route
export const getHealthRoute: Route<RequestSchema> = {
  method: "GET",
  url: "/api/v1/health",
  schema: requestSchema,
  handler: getHealthHandler,
};
