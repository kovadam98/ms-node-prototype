import { RequestSchema, requestSchema } from "./schema";
import { Handler, Route } from "../../../components/fastify/schema";
import { Card } from "../../persistence/model";
import redisService from "../../../components/redis/service";

// PreHandler
export const setCardStatusPreHandler: Handler<RequestSchema> = async (
  request,
  reply
) => {
  // Validate jwt token, request body, headers, etc. here...
  console.log("Pre handler");
  console.log(request.headers.authorization);
};

// Handler
export const setCardStatusHandler: Handler<RequestSchema> = async (
  request,
  reply
) => {
  // Write the logic here...
  const { cardToken, cardStatus } = request.body;
  const card = new Card({ cardToken, cardStatus });
  await redisService.set(card._id, card);
  card.save();
  reply.send({ transactionId: card._id });
};

// Route
export const setCardStatusRoute: Route<RequestSchema> = {
  method: "POST",
  url: "/api/v1/setCardStatus",
  schema: requestSchema,
  preHandler: setCardStatusPreHandler,
  handler: setCardStatusHandler,
};
