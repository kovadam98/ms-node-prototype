import { Static, Type } from "@sinclair/typebox";
import { RouteGenericInterface } from "fastify/types/route";

// This is necessary

enum CardStatus {
  active = "active",
  blocked = "blocked",
  frozen = "frozen",
}

const requestBody = Type.Object({
  cardStatus: Type.Enum(CardStatus),
  cardToken: Type.String(),
});

const response = Type.Object({
  transactionId: Type.String(),
});

const requestHeaders = Type.Object({
  Authorization: Type.String(), // make it required
});

// This is template

export const requestSchema = {
  body: requestBody,
  headers: requestHeaders,
  response: {
    200: response,
  },
};

export interface RequestSchema extends RouteGenericInterface {
  Body: Static<typeof requestBody>;
  Headers: Static<typeof requestHeaders>;
  Response: Static<typeof response>;
}
