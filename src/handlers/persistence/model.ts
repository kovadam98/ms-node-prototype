import { Schema, model } from "mongoose";

const CardSchema = new Schema({
  cardToken: { type: String, unique: true },
  cardStatus: { type: String },
});

export const Card = model("Card", CardSchema);
