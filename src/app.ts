import fastifyService, { logger } from "./components/fastify/service";
import { setCardStatusRoute } from "./handlers/api/set-card-status/route";
import { getHealthRoute } from "./handlers/api/health/route";
import mongoService from "./components/mongo/service";
import redisService from "./components/redis/service";
import { getTransactionStatusRoute } from "./handlers/api/get-transaction-result/route";

const startApp = async () => {
  try {
    logger.info("Starting fastify...");

    const routes = [
      setCardStatusRoute,
      getHealthRoute,
      getTransactionStatusRoute,
    ];
    await mongoService.start();
    await redisService.connect();
    await fastifyService.start(routes);
  } catch (error) {
    logger.error("Error while starting the service.");
    logger.error({ error });

    process.exit(1);
  }
};

const stopApp = async () => {
  try {
    await mongoService.stop();
    await redisService.disconnect();
    await fastifyService.stop();

    process.exit(0);
  } catch (error) {
    logger.error("Error while stopping the service.");
    logger.error({ error });

    process.exit(1);
  }
};

(async () => {
  await startApp();
})();

// CTRL+C - interrupt
process.on("SIGINT", stopApp);

// Docker stop - terminate
process.on("SIGTERM", stopApp);
