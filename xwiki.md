## Node tools, technologies, best practices

---

NodeJs is a runtime environment (like JVM or CLR) that can execute server-side JavaScript code.
It's ideal for microservice based projects, because it's scalable and easy to use,
but on the other hand maintaining it can be difficult.

The libraries are opensource, maintained by individuals or communities, so they can get deprecated, also JS is dynamically typed so the code can get messy if we don't use some tools and follow certain rules.

This description is a summary of the NodeJS tools that are being used in the MBH project combined with some explanation, best practices and an [example node app](https://bitbucket.org/kovadam98/ms-node-prototype/src/main/).

### Best practices

[This GitHub repo](https://github.com/goldbergyoni/nodebestpractices) is an organized overview of many node related software development ideas (project structure, testing, code style, etc...).  
There is a TL;DR part of every topic in a few paragraphs (max 30 mins to read it all) and also there is a link below every topic where you can read about the details.

### Static typing - TypeScript

With the use of TypeScript we can write statically typed code (\*.ts extension) that transpiles to JavaScript. We can get rid of the type errors in build time with it, many javascript libraries have TS support.  
I suggest using it throughout the project.  
We can configure it in the **tsconfig.json** file (EcmaScript version, build directory, etc...)  
**Disclaimer**: NodeJS runs JS code, so we have to build it before we can run it
[3 mins overview](https://javascript.plainenglish.io/typescript-101-understanding-the-basics-in-3-minutes-e2113a9c4c5f)

### Git hooks - Husky

Husky is an easy-to-use tool to run scripts triggered by git events. Currently, we use it for 3 hooks:

- pre-commit: auto code formatting. [(See below)](#formatting-code-styling---prettier)
- commit-msg: commit message linting. [(See below)](#commit-lint)
- pre-push: build & test.

It helps us to keep our commits, commit messages and checked-in code well-structured.
You can find the scripts in the **.husky** folder, it's usually 1-2 npm/npx commands.

### Code quality - ESLint

ESLint is a static code analysis tool used for catching bugs in the code. It integrates well with IDEs, so you can see warns while coding.  
It also detects formatting, code styling rule violations, but we use another tool for that.
We can configure it in the **.eslintrc.json** file.  
[Getting started](https://eslint.org/docs/user-guide/getting-started)

### Formatting, Code Styling - Prettier

Prettier is a code formatter tool for .{js,ts,json} files. We use it as the main code formatter for our projects. A git hook triggers it just before committing, so the committed codes will always follow the predefined rules, and we don't have to pay attention to that.  
ESLint and prettier complement each other, so we use both, check out:  
[Prettier vs ESLint](https://enlear.academy/eslint-vs-prettier-57882d0fec1d)

### lint staged

A config, to not just format the code but also add the formatted part to the git staging step. [Read more](https://www.npmjs.com/package/lint-staged)  
(If we didn't use this, the code formatting would still be triggered, but those changes wouldn't be a part of the new commit.)

### commit lint

A config to force a conventional commit message style. [Read more](https://commitlint.js.org/#/)  
current conventions in the project:  
**< type >: < message > (#< ticket-number >)**

examples:

**chore: docker setup (#107)**  
**fix: eslint rules (#112)**  
**refactor: token-generator component (#122)**

## Do these steps to get familiar with the git hooks:

Clone the [demo repository](https://bitbucket.org/kovadam98/ms-node-prototype/src/main/).

### pre-commit

1. Put some extra new lines, spaces in the src/app.ts file
2. Git add . and commit
3. You should see, that prettier formatted the code after your commit command

### commit-msg

unhappy path:

1. Add some code
2. Write a random commit message like this: **Add new features...**
3. Save the commit
4. The commit process should fail because of the commit message format is not appropriate

happy path:

1. Add some code
2. Write a commit message like this: **feat: add new features (#807)**
3. Save the commit
4. The commit process should succeed

### pre-push

Unhappy path:

1. Uncomment the faulty commit message in the test/app.test.ts file
2. Commit
3. Push origin main:random-branch
4. You should see that the code builds but the tests fail, so nothing is pushed

Happy path:

1. Delete the faulty commit message in the test/app.test.ts file
2. Commit
3. Push origin main:random-branch
4. You should see that the code builds and the tests succeed so the commit is pushed to the branch
