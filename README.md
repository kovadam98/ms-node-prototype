# Node prototype project

---

## Install

- npm install
- npm run prepare

## Build

- npm run build

## Run

- npm start

## Test

- npm test
